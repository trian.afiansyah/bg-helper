let chai = require('chai');
let moment = require('moment')
let h = require('../index.js')

describe('checking ',()=>{
    let kosong = ''
    let kosong1 = undefined
    let kosong2 = null
    let kosong3 = []
    let isi = 'isi'
    let isi1 = 123
    let isi2 = [1,2,3]

    let result = false
    let resultISI = false

    it('should return true cz variable value is empty',()=>{
        if (h.checkNullQueryAll(kosong)||h.checkNullQueryAll(kosong1)||h.checkNullQueryAll(kosong2)||h.checkNullQueryAll(kosong3)) {
            result = true
        }
        chai.expect(result).to.be.true
    })
    it('should return true cz variable value is empty and check it multiple',()=>{
        if (h.checkNullQueryAllExtended([kosong,kosong1,kosong2,kosong3],'or')) {
            result = true
        }
        chai.expect(result).to.be.true
    })
    it('should return true cz variable value is empty and check it multiple',()=>{
        if (h.checkNullQueryAllExtended([kosong,kosong1,kosong2,kosong3],'and')) {
            result = true
        }
        chai.expect(result).to.be.true
    })
    it('should return false cz variable value is not empty and check it multiple',()=>{
        if (h.checkNullQueryAllExtended([isi,isi1,isi2],'or')) {
            resultISI = true
        }
        chai.expect(resultISI).to.be.false
    })
})